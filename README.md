# Miscellaneous-College-Programs
Miscellaneous programs made for relating to college courses that I don't feel need to be hidden behind private repostitories or contained in dedicated repositories.

These programs may be well written or just simply thrown together to accomplish a task.

I would open source more of my college programs though I leave them hidden for academic integrity purposes. I don't want somebody in a class to find my GitHub and copy my work in the case that the course material is the same/similar.
