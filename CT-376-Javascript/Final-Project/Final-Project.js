/*
    Author: Maxwell Rosenthal
    Assignment: Final Project
    Course: CS-376: Javascript
    Date: March 27, 2022
    Description: Javascript code to handle the behavior of the Final-Project.html file.
 */

// Global declarations and initializations.

/**
 * cardArray consists of 52 strings.
 * These 52 strings are formatted in an XYZ-type format.
 * Expanding on this:
 * X will range from zero to three representing selection and flipped status:
 *      • 0 = Unflipped
 *      • 1 = Flipped
 *      • 2 = Unflipped and Selected
 *      • 3 = Flipped and Selected
 * Y will range from zero to three representing each card suit:
 *      • 0 = Clubs
 *      • 1 = Diamonds
 *      • 2 = Hearts
 *      • 3 = Spades
 * Z will be a hexidecimal number representing the value of a card (for simplicity, this will start at two).
 */
let cardArray = new Array
(
    "002", // Two of Clubs.
    "003", // Three of Clubs.
    "004", // Four of Clubs.
    "005", // Five of Clubs.
    "006", // Six of Clubs.
    "007", // Seven of Clubs.
    "008", // Eight of Clubs.
    "009", // Nine of Clubs.
    "00A", // Ten of Clubs.
    "00B", // Jack of Clubs.
    "00C", // Queen of Clubs.
    "00D", // King of Clubs.
    "00E", // Ace of Clubs.
    "012", // Two of Diamonds.
    "013", // Three of Diamonds.
    "014", // Four of Diamonds.
    "015", // Five of Diamonds.
    "016", // Six of Diamonds.
    "017", // Seven of Diamonds.
    "018", // Eight of Diamonds.
    "019", // Nine of Diamonds.
    "01A", // Ten of Diamonds.
    "01B", // Jack of Diamonds.
    "01C", // Queen of Diamonds.
    "01D", // King of Diamonds.
    "01E", // Ace of Diamonds.
    "022", // Two of Hearts.
    "023", // Three of Hearts.
    "024", // Four of Hearts.
    "025", // Five of Hearts.
    "026", // Six of Hearts.
    "027", // Seven of Hearts.
    "028", // Eight of Hearts.
    "029", // Nine of Hearts.
    "02A", // Ten of Hearts.
    "02B", // Jack of Hearts.
    "02C", // Queen of Hearts.
    "02D", // King of Hearts.
    "02E", // Ace of Hearts.
    "032", // Two of Spades.
    "033", // Three of Spades.
    "034", // Four of Spades.
    "035", // Five of Spades.
    "036", // Six of Spades.
    "037", // Seven of Spades.
    "038", // Eight of Spades.
    "039", // Nine of Spades.
    "03A", // Ten of Spades.
    "03B", // Jack of Spades.
    "03C", // Queen of Spades.
    "03D", // King of Spades.
    "03E", // Ace of Spades.
); // End let cardArray = new Array.

let cardHandSelection = -1; // Establish a tracker for what card in their hand the player has selected. -1 means that no card is selected.
let cardGridSelection = -1; // Establish a tracker for what card in the grid the player has selected. -1 means that no card is selected.
let playerTurn = false; // Establish a tracker for whose turn it is. Boolean false is player one, true is player two.
let turnNumber = 1; // Establish a tracker for the turn number.
let timeOfLastTurnChange = new Date(); // Establish a tracker for the time in which the last turn change took place.
let hoverTimeTracker = new Date(); // Establish a traker for hover time over the hand of the person whose turn it isn't.
let music; // Declare a global variable for background music.
let playerWins = new Array(0, 0); // Establish an array that will be used to track player wins.
let dontHide = false; // Establish a tracker to prevent the player hand preview delay from being cut short.
let turnLimit = 0; // Establish the amount of turns that will take place before hand reveals.

const delay = ms => new Promise(res => setTimeout(res, ms)); // Establish a promise for delay in asynchronous JavaScript blocks.

// Functions.

/*****************************************
 * Initialization/Setup/General Functions.
 *****************************************/

/**
 * This function carries out the required intricate initializations of the program.
 */
function initialize()
{
    setup(true); // Run the setup function.

    // Import music.

    let rng = (Math.floor(Math.random() * 3));

    if (rng == 0) // 1/3 chance...
        music = new Audio("Assets/Music/BGM-1.mp3"); // Import BGM-1 for background music.

    else if (rng == 1) // 1/3 chance...
        music = new Audio("Assets/Music/BGM-2.mp3"); // Import BGM-2 for background music.

    else // 1/3 chance...
        music = new Audio("Assets/Music/BGM-3.mp3"); // Import BGM-3 for background music.
} // End function initialize().

/**
 * This function runs commands that setup (and subsequently reset) the game.
 * @param {boolean} initialization Boolean true or false indicating on whether or not the game is being called upon initialization respectively (defaults to false).
 */
function setup(initialization = false)
{
    const setupAsync = async () => // Asynchronously setup the game.
    {
        for (let i = 0; i < 1024; i++)  // Shuffle the array 1024 times.
            swap(Math.floor(Math.random() * 52), Math.floor(Math.random() * 52)); // Swap the cards of two randomly generated positions.

        for (let i = 0; i < 10; i++) // For the first 10 cards in the cardArray...
            cardArray[i] = '1' + cardArray[i].substring(1); // Set the cards to be flipped.

        sortHand(false); // Sort player one's hand.
        sortHand(true); // Sort player two's hand.

        document.getElementById("playerOneText").className = "playerTurn"; // Set the class name of playerOneText to playerTurn.
        document.getElementById("playerTwoText").className = ""; // Set the class name of playerTwoText to nothing.

        drawGrid(); // Draw the grid.
        hideHand(); // Draw the hands of the players as hidden cards.

        if (initialization) // If the function is being called via initialization()...
            await delay(250); // Pause for half a second.

        else // If the function is being called with the start of a new game...
            await delay(100); // Pause for 0.1 seconds.

        acquireTurnCount(); // Acquire the turn count.
    } // End const setupAsync = async () =>.

    setupAsync(); // Setup asynchronously.
} // End function setup().

/**
 * This function acquires the amount of turns the players will play for.
 */
function acquireTurnCount()
{
    turnLimit = 0; // Set turnLimit to zero.

    while (turnLimit < 1 || turnLimit > 21) // While turns is not within the range of how many cards are in the grid (divided by two)...
    {
        let input = prompt("How many turns do you want to play for?\n(1 - 21, 5 - 7 recommended)"); // Acquire user input.

        if (!isNaN(input) && Number.isInteger(parseFloat(input))) // If the input is a number and that number is an integer...
            turnLimit = parseInt(input); // Update turnLimit to the inputted value.
   } // End while (turnLimit < 1 || turnLimit > 21).

   document.getElementById("turnCap").innerHTML = turnLimit; // Update turnCap field to be equal to the hardcoded turn limit.
} // End function acquireTurnCount().

/**
 * This function swaps two cards in cardArray.
 * @param {int} a The index of the first card in the array to swap as an integer. 
 * @param {int} b The index of the second card in the array to swap as an integer.
 */
function swap(a, b)
{
    let temp = cardArray[a]; // Assign the card at index a to a temporary value.

    cardArray[a] = cardArray[b]; // Assign index a's card to be equal to index b's card.
    cardArray[b] = temp; // Assign the card at index b to be equal to the temporary value.
} // End function swap(a, b).

/**
 * This function sorts a player's hand using a selection sort.
 * @param {boolean} player The player as a boolean who will have their hand sorted.
 */
function sortHand(player)
{
    // Declarations and initializations.

    let index = 0; // Establish a starting index for the player.

    if (player) // If it is player two's turn...
        index = 5; // Set index to five as that is player two's starting index..

    let upperBound = index + 5; // Establish the upper bound for looping.

    // Execution.

    for(let i = index; i < upperBound; i++) // Loop through every index of the current player's hand.
    {
        let min = i; // Copy the current index to a minimum variable.

        for (let j = i + 1; j < upperBound; j++) // Loop through every index of the current player's hand after the current index.
            if(cardArray[j].charAt(2) > cardArray[min].charAt(2)) // If the value at the current index is less than the value of the minimum index...
                min = j; // Update the minimum index to be the current index.

        if (min != i) // If the minimum index is not the same as the index...
            swap(i, min); // Swap the current index's value with the minimum index's value.
    } // End for(let i = index; i < upperBound; i++).
} // End function sortHand(player).

/**
 * Displays the rules of the game in an alert box.
 */
function displayRules()
{
    window.alert("Improvised Browser Edition Rules:\n\n" +
        "Luck of the Draw is a game that consists of a deck of 52 cards and of said deck of cards, each player will have five random cards with the remaining cards being placed into a grid.\n\n" +
        "Whenever it is a player's turn to play, they have two options:\n" +
        "1. They may select a card on the grid and swap it with a card in their hand (or vice versa), revealing the card they placed onto the grid in the process.\n" +
        "2. They may select a card to reveal on the grid and skip their turn.\n\n" +
        "Once an agreed upon number of turns has passed, the winner will be determined by poker hand order. Aces are both high and low."
    ); // Print an abridged version of Rules.txt as an alert.
} // End function displayRules().

 /**
  * Plays and pauses the background music.
  * 
  * This would have been autoplayed though Google makes it so that one must interact with the browser first...
  * At least this allows me to have a play/pause button easter-egg.
  */
function playMusic()
{
    music.loop = true; // Make the background music looping.

    if(music.paused == false) // If the music is playing...
        music.pause(); // Pausie the music.

    else // Otherwise...
        music.play(); // Play the music.
} // End function playMusic().

/*****************
 * Grid Functions.
 *****************/

/**
 * This function handles the drawing of the card grid on the HTML webpage.
 * @param {boolean} flipped Whether or not the cards should be drawn as flipped as a boolean.
 */
function drawGrid(flipped = false)
{
    // Declarations and initializations.

    let index = 10; // Insertion will start at index 10 as the first 10 indexes (indexes zero through nine) are allocated to be the players' hands.
    let gridHTML = "<table>"; // Establish the string for the grid HTML to be generated.

    // Execution.

    for (let rows = 0; rows < 3; rows++) // Loop three times (once for each row).
    {
        gridHTML += "<tr>";
        for (let columns = 0; columns < 14; columns++) // Loop 14 times (once for each column).
        {
            if (flipped) // If the cards should be drawn as flipped...
                gridHTML += "<td><img " +
                    "id = \"" + index + "\" " +
                    "class = \"card\" " +
                    "src = \"Assets/Cards/" + cardArray[index].charAt(2) + "_of_" + cardArray[index].charAt(1) + ".png\" " +
                    "height = \"100pct\" " +
                    "onclick = \"gridInteract(" + index + ")\"/></td>"; // Render the card at the current index of the grid hand as flipped.

            else // If the cards should be drawn as unflipped...
                gridHTML += "<td><img " +
                    "id = \"" + index + "\" " +
                    "class = \"card\" " +
                    "src = \"Assets/Cards/back_red.png\" " +
                    "height = \"100pct\" " +
                    "onclick = \"gridInteract(" + index + ")\"/></td>"; // Render the card at the current index of the grid hand as unflipped.

            index++; // Increment the index value by one.
        } // End for (let columns = 0; columns < 7; columns++).
        gridHTML += "<tr/>"
    } // End for (let rows = 0; rows < 6; rows++).
    document.getElementById("cardGrid").innerHTML = gridHTML + "</table>"; // Update cardGrid within the HTML.
} // End drawGrid(flipped = false).

/**
 * Re-draws a card on the grid at a specified index.
 * @param {int} index The index of the card to be re-drawn as an integer.
 */
function drawIndex(index)
{
    if (cardArray[index].charAt(0) == '0') // If the card is unflipped...
    {
        document.getElementById(String(index)).className = "card"; // Change the class of the card.
        document.getElementById(String(index)).src = "Assets/Cards/back_red.png"; // Change the image of the card.
    } // End if (cardArray[index].charAt(0) == '0').

    else if (cardArray[index].charAt(0) == '1') // If the card is flipped...
    {
        document.getElementById(String(index)).className = "card"; // Change the class of the card.
        document.getElementById(String(index)).src = "Assets/Cards/" + cardArray[index].charAt(2) + "_of_" + cardArray[index].charAt(1) + ".png"; // Change the image of the card.
    } // End else if (cardArray[index].charAt(0) == '1').

    else if (cardArray[index].charAt(0) == '2') // If the card is selected and unflipped...
    {
        document.getElementById(String(index)).className = "cardSelected"; // Change the class of the card.
        document.getElementById(String(index)).src = "Assets/Cards/back_red.png"; // Change the image of the card.
    } // End else if (cardArray[index].charAt(0) == '2').

    else // If the card is selected and flipped...
    {
        document.getElementById(String(index)).className = "cardSelected"; // Change the class of the card.
        document.getElementById(String(index)).src = "Assets/Cards/" + cardArray[index].charAt(2) + "_of_" + cardArray[index].charAt(1) + ".png"; // Change the image of the card.
    } // End else.
} // End function drawIndex(index).

/**
 * This function handles the interaction between the player and the grid of cards.
 * @param {int} index The card interacted with's index in the cardArray as an integer. 
 */
function gridInteract(index)
{
    if ((new Date()).getTime() < timeOfLastTurnChange.getTime() + 1250 &&
        (playerTurn == true || turnNumber != 1)) // If the player is interacting with the card grid within 1.25 seconds and it is not the first turn of a game with the first player up...
        return; // Stop execution of the function.

    else if (cardGridSelection != -1) // If a card in the grid is currently selected...
    {
        cardArray[cardGridSelection] = String(parseInt(cardArray[cardGridSelection].charAt(0)) - 2) + cardArray[cardGridSelection].substring(1); // Deselect the to-be previous selected card.

        drawIndex(cardGridSelection); // Redraw the previously selected card.

        if (cardGridSelection == index) // If the currently selected card is the same as the to-be selected card...
        {
            cardGridSelection = -1; // Deselect the previously selected card.

            return; // Stop execution of the function.
        } // End if (cardGridSelection == index).
    } // End if (cardGridSelection != -1).

    cardArray[index] = String(parseInt(cardArray[index].charAt(0)) + 2) + cardArray[index].substring(1); // Select the card at the selected index.
    cardGridSelection = index; // Update the cardGridSelection to be the current index.

    if (cardHandSelection != -1) // If there is a card selected within the player's hand.
    {
        changeTurns(false); // Change turns.
        return; // Stop execution of the funciton.
    } // End if (cardHandSelection != -1).

    drawIndex(cardGridSelection); // Draw the currently selected card.
} // End function gridInteract(index).

/*****************
 * Hand Functions.
 *****************/

/**
 * This function handles the hiding aspect of drawing player hands on the HTML webpage.
 * @param {boolean} hover Whether or not the function is being called by an onmouseover event (defaults boolean false).
 * @param {int} index The player who is calling the function (defaults to -1 to indicate that both hands need to be drawn).
 */
function hideHand(hover = false, index = -1)
{
    if (dontHide || cardHandSelection != -1) // If function is called and the hand should not be hidden or something in the hand is selected...
        return; // Stop execution of the function.

    else if (hover && ((playerTurn == false && index == 1) || (playerTurn == true && index == 0))) // If a player is hovering over another player's cards...
        hoverTimeTracker = new Date(); // Assign the hoverTimeTracker to the current date/time.

    // Declarations and initializations.

    let upperBound = -1; // Establish a value to keep track of what the upper bound for the command should be.

    if (index == 0) // If playerOne's hand is being hidden...
        upperBound = 1; // Set the loop's upperBound to one.

    else
    {
        upperBound = 2; // Set the upperBound to two.

        if (index == -1) // If the default case is present...
            index = 0; // Set the index value to zero.
    } // End else.

    // Execution.

    for (; index < upperBound; index++) // Loop from index to upperBound...
    {
        let handHTML = "<table><tr>"; // Establish the string for the hand HTML to be generated.

        if (!hover) // If the player's hand is being rendered via unhovering/other means...
            for (let card = 0; card < 5; card++) // Loop five times (once for every card).
                handHTML += "<td><img " +
                    "class = \"handCard\" " +
                    "src = \"Assets/Cards/back_red.png\" " +
                    "height = \"100pct\" " +
                    "onmouseover = \"hideHand(true, " + index + ")\"/></td>"; // Render the card at the current index in the player's hand as unflipped.

        else if ((playerTurn == false && index == 1) || (playerTurn == true && index == 0)) // If the player whose turn it isn't is having their hand hovered over...
            for (let card = 0; card < 5; card++) // Loop five times (once for every card).
                handHTML += "<td><img " +
                    "class = \"handCardOpaque\" " +
                    "src = \"Assets/Cards/back_red.png\" " +
                    "height = \"100pct\" " +
                    "onmouseout = \"hideHand(false, " + index + ")\" " +
                    "onclick = \"viewHand(" + index + ", false)\"/></td>"; // Render the card at the current index in the player's hand as unflipped with a selectable highlight and onclick behavior indicating that the cards don't belong to the current player.

        else // If the player's hand is being hovered over...
            for (let card = 0; card < 5; card++) // Loop five times (once for every card).
                handHTML += "<td><img " +
                    "class = \"handCardOpaque\" " +
                    "src = \"Assets/Cards/back_red.png\" " +
                    "height = \"100pct\" " +
                    "onmouseout = \"hideHand(false, " + index + ")\" " +
                    "onclick = \"viewHand(" + index + ")\"/></td>"; // Render the card at the current index in the player's hand as unflipped with a selectable highlight.

        if (index == 0) // If player one...
            document.getElementById("playerOneHand").innerHTML = handHTML + "</tr></table>"; // Update playerOneHand within the HTML.

        else // If player two...
            document.getElementById("playerTwoHand").innerHTML = handHTML + "</tr></table>"; // Update playerTwoHand within the HTML.
    } // End for (; index < upperBound; index++).
} // End function hideHand().

/**
 * This function handles the viewing aspect of drawing the player's hand on the HTML webpage.
 * @param {int} player The player who is calling the function (defaults to -1 to indicate that both hands need to be drawn).
 * @param {boolean} currentPlayer Whether or not the hand attempting to be viewed belongs to the current player.
 */
function viewHand(player = -1, currentPlayer = true)
{
    if ((currentPlayer && (new Date()).getTime() >= timeOfLastTurnChange.getTime() + 1250) ||
        (!currentPlayer && (new Date()).getTime() >= hoverTimeTracker.getTime() + 1250) ||
        player == -1 ||
        (playerTurn == false && turnNumber == 1 && currentPlayer == true)) // If 1.25 seconds have passed since the last turn or the player whose turn it isn't has been hovered over for 1.25 seconds or it's the game calling the funciton generally or it's the turn of the first player on the first turn and said first player is viewing their hand...
    {
        // Declarations and initializations.

        let upperBound = -1; // Establish a value to keep track of what the upper bound for the command should be.

        if (player == 0) // If playerOne's hand is being hidden...
            upperBound = 1; // Set the loop's upperBound to one.
    
        else
        {
            upperBound = 2; // Set the upperBound to two.
    
            if (player == -1) // If the default case is present...
                player = 0; // Set the player value to zero.
        } // End else.

        // Execution.

        for (; player < upperBound; player++) // Loop from index to upperBound...
        {
            let index = player * 5; // Obtain the starting index of the player.
            let handHTML = "<table><tr>"; // Establish the string for the hand HTML to be generated.

            for (let card = 0; card < 5; card++) // Loop five times (once for every card).
            {
                if (currentPlayer) // If it is the current player's turn...
                    if (cardArray[index].charAt(0) == '3') // If the card is selected...
                        handHTML += "<td><img " +
                        "class = \"cardSelected\" " +
                        "src = \"Assets/Cards/" + cardArray[index].charAt(2) + "_of_" + cardArray[index].charAt(1) + ".png\" " +
                        "height = \"100pct\" " +
                        "onclick = \"handInteract(" + index + ", " + player + ")\"/></td>" // Render the card at the current index of the player's hand as flipped with indication that it is selected.
        
                    else // If the card isn't selected...
                        handHTML += "<td><img " +
                        "class = \"card\" " +
                        "src = \"Assets/Cards/" + cardArray[index].charAt(2) + "_of_" + cardArray[index].charAt(1) + ".png\" " +
                        "height = \"100pct\" " +
                        "onclick = \"handInteract(" + index + ", " + player + ")\"/></td>" // Render the card at the current index of the player's hand as flipped.
 
                else // If it is not the current player's turn...
                    handHTML += "<td><img " +
                    "class = \"card\" " +
                    "src = \"Assets/Cards/" + cardArray[index].charAt(2) + "_of_" + cardArray[index].charAt(1) + ".png\" " +
                    "height = \"100pct\"/></td>" // Render the flipped card at the current index of the player whose turn it isn't without clicking behavior.

                index++; // Increment the value of index.
            } // End for (let rows = 0; rows < 5; rows++)
    
            if (player == 0) // If it's player one's turn...
                document.getElementById("playerOneHand").innerHTML = handHTML + "</tr></table>"; // Update playerOneHand within the HTML.
    
            else
                document.getElementById("playerTwoHand").innerHTML = handHTML + "</tr></table>"; // Update playerOneHand within the HTML.
        } // End for (; player < upperBound; index++).
    }
} // End function drawHand().

/**
 * This function handles the interaction between the player and their hand of cards.
 * @param {int} index The card interacted with's index in the cardArray as an integer. 
 * @param {int} player The player who is calling the function.
 */
function handInteract(index, player)
{
    if (cardHandSelection != -1) // If a card in the player's hand is currently selected...
    {
        cardArray[cardHandSelection] = String(parseInt(cardArray[cardHandSelection].charAt(0)) - 2) + cardArray[cardHandSelection].substring(1); // Deselect the to-be previous selected card.

        if (cardHandSelection != index) // If the card selected isn't the same as the to-be selected card...
            swap(cardHandSelection, index); // Change the ordering of the cards within the player's hand.

        cardHandSelection = -1; // Deselect the previously selected card.

        viewHand(player); // View the player's hand again.

        return; // Stop execution of the function.
    } // End if (cardHandSelection != -1).

    cardArray[index] = String(parseInt(cardArray[index].charAt(0)) + 2) + cardArray[index].substring(1); // Select the card at the selected index.
    cardHandSelection = index; // Update the cardHandSelection to be the current index.

    if (cardGridSelection != -1) // If there is a card selected within the grid.
    {
        changeTurns(false); // Change turns.
        return; // Stop execution of the funciton.
    } // End if (cardGridSelection != -1).

    viewHand(player); // View the player's hand again.
} // End function handInteract(index).

/*****************************
 * Game progression Functions.
 *****************************/

/**
 * This function handles the behavior of updating whose turn it is and general turn progression.
 */
function changeTurns(passed)
{
    if (passed) // If the player passed...
        if ((new Date()).getTime() < timeOfLastTurnChange.getTime() + 1250) // If 1.25 seconds haven't passed since the last turn change...
            return; // End the function's execution.

        else if (cardGridSelection == -1 || cardArray[cardGridSelection].charAt(0) !== '2') // If the player hasn't selected a card within the grid or the card selected is already flipped...
        {
            if (!playerTurn || turnNumber != turnLimit) // If it is not player two's turn on the last turn of the game...
            {
                window.alert("You must select an unflipped card within the grid to be flipped before you can pass your turn."); // Tell the player to select a card.
                return; // End the function's execution.    
            } // End if (!playerTurn || turnNumber != turnLimit).
        } // End if (cardGridSelection = -1 || cardArray[cardGridSelection].charAt(0) !== '2').

        else
        {
            cardArray[cardGridSelection] = '1' + cardArray[cardGridSelection].substring(1); // Flip the selected card on the grid.

            drawIndex(cardGridSelection); // Redraw the previously selected card.
        } // End else.

    else // If the player is swapping cards.
    {
        cardArray[cardHandSelection] = '1' + cardArray[cardHandSelection].substring(1); // Deselect the selected card in the player's hand.
        cardArray[cardGridSelection] = '1' + cardArray[cardGridSelection].substring(1); // Deselect/Flip the selected card on the grid.

        swap(cardHandSelection, cardGridSelection); // Swap the selected card in the player's hand with the selected card in the grid.

        drawIndex(cardGridSelection); // Draw the card at the selected grid index.
    } // End else.

    playerTurn = !playerTurn; // Change whose turn it is.

    if (playerTurn == false) // If it is player one's turn...
    {
        turnNumber++; // Increment the turn number.

        if (turnNumber > turnLimit) // If the turn limit has been exceeded...
        {
            turnNumber = 1; // Reset back to turn one.

            concludeRound(); // Conclude the round.
        } // End if (turnNumber > turnLimit).

        else // If the turn limit has not been exceeded...
            document.getElementById("turn").innerHTML = turnNumber; // Update the turn display to indicate what turn it is.
    } // End if (playerTurn == false).

    cardHandSelection = -1; // Reset the card selection within the player's hand.
    cardGridSelection = -1; // Reset the card selection within the grid.
    timeOfLastTurnChange = new Date() // Update the time in which the last turn change took place.

    if (turnNumber == 1 && !playerTurn) // If it is player one's turn on the first turn (if the game was reset)...
        return; // End the function's execution.

    if (!passed) // If the player did not pass on their turn...
    {
        const showHideUpdated = async () => // Asynchronously show and hide the updates.
        {
            let handHTML = "<table><tr>" // Establish the string for the hand HTML to be generated.
            let index = 0; // Establish a starting index for the player.
            let upperBound = 5; // Establish an upper bound for the player.

            if (!playerTurn) // If player two was just up...
            {
                index = 5; // Assign the starting index to player two's starting index.
                upperBound = 10; // Assign the upper bound to player two's upper bound.
            } // End if (!playerTurn).

            dontHide = true; // Set dontHide to true.

            for (; index < upperBound; index++) // Loop through the player's hand.
            {
                handHTML += "<td><img " +
                "class = \"handCard\" " +
                "src = \"Assets/Cards/" + cardArray[index].charAt(2) + "_of_" + cardArray[index].charAt(1) + ".png\" " +
                "height = \"100pct\"</td>" // Render the card at the current index of the grid as flipped.
            } // End for (let index = 0; index < 5; index++).

            if (playerTurn) // If player one was just up...
                document.getElementById("playerOneHand").innerHTML = handHTML + "</tr></table>"; // Update playerOneHand within the HTML.

            else // If player two was just up...
                document.getElementById("playerTwoHand").innerHTML = handHTML + "</tr></table>"; // Update playerOneHand within the HTML.            

            await delay(1250); // Pause for 1.25 seconds.

            dontHide = false; // Set dontHide to false.

            if (playerTurn) // If player one was just up...
            {
                hideHand(false, 0); // Hide player one's hand.
                document.getElementById("playerOneText").className = ""; // Set the class name of playerOneText to nothing.
                document.getElementById("playerTwoText").className = "playerTurn"; // Set the class name of playerTwoText to playerTurn.
            } // End if (playerTurn).

            else // If player two was just up...
            {
                hideHand(false, 1); // Hide player two's hand.
                document.getElementById("playerOneText").className = "playerTurn"; // Set the class name of playerOneText to playerTurn.
                document.getElementById("playerTwoText").className = ""; // Set the class name of playerTwoText to nothing.
            } // End else.
        }; // End const showHideUpdated = async () =>.

        showHideUpdated(); // Call showHideUpdated.
    } // End if (!passed).

    else // If the player passed on their turn...
        if (playerTurn) // If player one was just up...
        {
            document.getElementById("playerOneText").className = ""; // Set the class name of playerOneText to nothing.
            document.getElementById("playerTwoText").className = "playerTurn"; // Set the class name of playerTwoText to playerTurn.
        } // End if (playerTurn).

        else // If player two was just up...
        {
            hideHand(false, 1); // Hide player two's hand.
            document.getElementById("playerOneText").className = "playerTurn"; // Set the class name of playerOneText to playerTurn.
            document.getElementById("playerTwoText").className = ""; // Set the class name of playerTwoText to nothing.
        } // End else.
} // End function changeTurns(passed).

/**
 * This function concludes the round by announcing the victor and performing general reset functionality.
 */
function concludeRound()
{
    // Declarations and initializations.

    let victorString = ""; // Begin the string to be outputted as an alert window.
    let playerOneScore = getScore(false); // Obtain player one's score.
    let playerTwoScore = getScore(true); // Obtain player two's score.
    let previousScores = playerWins.slice(); // Copy the current values of playerWins to a previousScores varaible.

    // Execution.

    // Determine the victor.

    if (playerOneScore > playerTwoScore) // If player one won...
        playerWins[0]++; // Increment the win counter for player one.

    else if (playerOneScore < playerTwoScore) // If player two won...
        playerWins[1]++; // Increment the win counter for player two.

    else if (playerOneScore > 600) // If a full house or above tie occurred (not all cases such as the one mentioned will trigger this)...
        ; // Do nothing.

    else if (playerOneScore > 500) // If the players obtained flushes...
        highCardTieBreak(); // Attempt to break the flush tie by performing a high card tie break.

    else if (playerOneScore > 300) // If a three of a kind or above tie occurred...
        ; // Do nothing.

    else // If the players tied through any other means...
        if (playerOneScore > 200) // If the players obtained two pairs...
            twoPairTiebreak(playerOneScore); // Attempt to break the two pair tie.

        else if (playerOneScore > 100) // If the players obtained pairs...
            pairTieBreak(playerOneScore); // Attempt to break the pair tie.

        else // If the players had high cards...
            highCardTieBreak(); // Attempt to break the high card tie.

    // Concatonate string information.

    if (playerWins[0] == previousScores[0] && playerWins[1] == previousScores[1]) // If the game was tied...
        victorString += "The game ended in a draw!"; // Append text indicating a draw.

    else // If the game was not tied...
    {
        if (playerWins[0] > previousScores[0]) // If player one won...
            victorString += "The winner of this game is player one!"; // Append text indicating that player one won.

        else // If player two won...
            victorString += "The winner of this game is player two!"; // Append text indicating that player two won.
    } // End else.

    victorString += "\nPlayer one wins: " + playerWins[0] +
        "                     Player two wins: " + playerWins[1];

    const displayResults = async () => // Asynchronously show the results.
    {
        dontHide = true; // Set dontHide to true.

        viewHand(); // View both of the players' hands.
        drawGrid(true); // Draw the grid as unflipped cards.

        await delay(100); // Pause for 0.1 seconds.

        dontHide = false; // Set dontHide to false.

        window.alert(victorString); // Print victorString.

        for (let i = 0; i < 52; i++) // For every card in cardArray...
            cardArray[i] = '0' + cardArray[i].substring(1); // Set the card to be unflipped.

        document.getElementById("turn").innerHTML = '1'; // Update the turn display to indicate that it is turn one.
        document.getElementById("turnCap").innerHTML = '?'; // Update turnCap field to be a question mark.

        setup(); // Call the setup function.
    }; // End const displayResults = async () =>.

    displayResults(); // Call displayResutls.
} // End function concludeRound()

/**
 * This function returns the score of the player in poker order.
 * These returned scores are formatted in a XYY-type format.
 * Expanding on this:
 * X represents the winning hand type:
 *      • 0 - High Card
 *      • 1 - Pair
 *      • 2 - Two Pair
 *      • 3 - Three of a Kind
 *      • 4 - Straight
 *      • 5 - Flush
 *      • 6 - Full House
 *      • 7 - Four of a Kind
 *      • 8 - Straight Flush
 *      • 9 - Royal Flush
 * YY simply is the representation of the hand's priority value in decimal form (ex. 12 = Queen, this may be marked as zeroes for royal flushes, a high card, or some other card as need requires).
 * In the case of High Card, Pair, and Two Pair, additional processing will be needed upon comparison between scores to address any potential ties.
 * @param {boolean} player the player to get the score of as a boolean
 */
function getScore(player)
{
    // Declarations and initializations.

    let index = 0; // Establish a starting index for the player.

    if (player) // If it is player two's turn...
        index = 5; // Set index to five as that is player two's starting index..

    let upperBound = index + 5; // Establish the upper bound for the current player.
    let returnValue = 0; // Establish a tracker for the value to return.


    // Execution.

    sortHand(player); // Sort the player's hand.

    returnValue = checkFlushes(index, upperBound); // Check for anything related to flushes.

    if (returnValue > 800) // If a straight flush or a royal flush occurred...
        return returnValue; // Return the value stored in returnValue.

    returnValue = checkThreePlus(index, upperBound, returnValue);// Check for whether or not the player has a four of a kind, full house, or three of a kind.

    if (returnValue != 0) // If returnValue has been updated...
        return returnValue; // Return the value stored in returnValue.

    if (checkStraight(index, upperBound)) // If the player's hand is a straight...
        return returnStraight(index, upperBound, 400); // Call and return the value returned by the returnStraight function.

    // Two Pair and Pair checks.

    returnValue = pairCheck(index, upperBound)

    if (returnValue != 0) // If returnValue has been updated...
        return returnValue; // Return the value stored in returnValue.

    // Return the high card.

    return obtainCardValue(index); // Return the value of the card at the starting index to indicate that the player had a high card hand.
} // End function getScore(player).

/**
 * This function checks for anything related to flushes for a player.
 * @param {int} index The starting index of a player's hand.
 * @param {int} upperBound The upper bound index of a player's hand.
 * @returns The hand of a player numerically represented.
 */
function checkFlushes(index, upperBound)
{
    // Declarations and initializations.

    let valueTracker = cardArray[index].charAt(1); // Copy the suite of the first card to a value tracking variable.
    let handTracker = true; // Establish a tracker for whether or not the player has obtained a flush.

    // Execution.

    for (let i = index + 1; i < upperBound; i++) // Loop through the last four cards of the player's hand.
        if (cardArray[i].charAt(1) != valueTracker) // If the current index's suite isn't the same as that of the first card...
        {
            handTracker = false; // Update the status of the tracker to indicate that the hand isn't a flush.
            break; // Stop executing the search.
        } // End if (cardArray[i].charAt(1) != flushValue).

    if (handTracker) // If the hand is a flush.
    {
        // Check for a royal flush.

        for (let i = index; i < upperBound; i++) // Loop through the player's hand.
            if (cardArray[i].charCodeAt(2) != (69 - (i % 5))) // If the order is not in an ace-descending order.
            {
                handTracker = false; // Update the status of the tracker to indicate that the hand isn't a royal flush.
                break; // Stop looping.
            } // End if (cardArray[index].charCodeAt(2) != (69 - i)).

        if (handTracker) // If the player's hand is a royal flush.
            return 900; // Return 900 to indicate that the player had a royal flush.

        // Check for a straight flush.

        if (checkStraight(index, upperBound)) // If the player's hand is a straight flush...
            return returnStraight(index, upperBound, 800); // Call and return the value returned by the returnStraight function.

        // Record the occurance of a flush.

        return 500 + obtainCardValue(index); // Return 5YY to indicate that the player has obtained a flush.
    } // End if (handTracker).

    return 0; // Return zero to indicate that nothing related to flushes occurred.
} // End function checkFlushes(index, upperBound).

/**
 * This function checks for whether or not the player has a four of a kind, full house, or three of a kind.
 * @param {int} index The starting index of a player's hand.
 * @param {int} upperBound The upper bound index of a player's hand.
 * @param returnValue The value that will be returned (it is being passed in just in case a flush has occurred, the default case will be zero).
 * @returns The hand of a player numerically represented.
 */
function checkThreePlus(index, upperBound, returnValue = 0)
{
    // Declarations and initializations.

    let valueTracker = cardArray[index].charAt(2); // Copy the value of the first card to a value tracking variable.
    let repeatCounter = 1; // Establish a tracker for the number of repeated cards.
    let secondaryRepeatCounter = 0; // Establish secondary repeat tracking variable for if it is required.
    let tetrinaryRepeatCounter = 0; // Establish a tracker to be used in determining repeat values for full houses.

    // Execution.

    for (let i = index + 1; i < upperBound; i++) // Loop through the last four cards of the player's hand.
    {
        // Handle repeatCounter.

        if (cardArray[i].charAt(2) == valueTracker) // If the value of the card at the current index is equal to that of valueTracker.
            repeatCounter++; // Increase the repeat counter.

        else // if the card wasn't repeated...
        {
            if (i == (upperBound - 3) && secondaryRepeatCounter == 2) // If if is the third index and two cards were just repeated...
                tetrinaryRepeatCounter = 2; // Set tetrinaryRepeatCounter to two.

            repeatCounter = 1; // Reset the repeat counter.

            valueTracker = cardArray[i].charAt(2); // Copy the value of the first card to the value tracking variable.
        } // End else.

        // Check for if the player has a full house or a three of a kind.

        if (repeatCounter == 2) // If two cards have been repeated...
            if (secondaryRepeatCounter == 3) // If there has already been three cards repeated...
                return 600 + obtainCardValue(index) // Return 6YY to indicate that the player has a full house.

            else // If this is the first occurrance of two repeated cards...
                secondaryRepeatCounter = 2; // Set secondaryRepeatCounter to two.

        else if (repeatCounter == 3) // If three cards have been repeated...
            if (tetrinaryRepeatCounter == 2) // If the player already has found a pair...
                return 600 + obtainCardValue(i) // Return 6YY to indicate that the player has a full house.

            else if (secondaryRepeatCounter == 2 && i == (upperBound - 3)) // If secondaryRepeatCounter is two and it is the third index...
                secondaryRepeatCounter = 3; // Set secondaryRepeatCounter to three.

            else if (i == (upperBound - 2)) // If it is the fourth index...
                tetrinaryRepeatCounter = 3;

            else // If it is the fifth index...
                if (returnValue != 0) // If a flush occurred...
                    return returnValue; // Return the value stored in returnValue.

                else // If a flush didn't occur...
                    return 300 + obtainCardValue(i); // Return 3YY to indicate that the player has a three of a kind.

        else if (repeatCounter == 4) // If four cards have been repeated...
            return 700 + obtainCardValue(i); // Return 7YY to indicate that the player has a four of a kind.

        else if (tetrinaryRepeatCounter == 3 || (i == (upperBound - 1) && secondaryRepeatCounter == 3)) // If tetrinaryRepeatCounter is three or it is the last index and three cards were repeated...
            if (returnValue != 0) // If a flush occurred...
                return returnValue; // Return the value stored in returnValue.

            else // If a flush didn't occur...
                return 300 + obtainCardValue(index); // Return 3YY to indicate that the player has a three of a kind.
    } // End for (let i = index + 1; i < upperBound; i++).

    return returnValue; // Return returnValue to indicate that nothing occurred that should modify the current value of returnValue.
} // End function checkThreePlus(index, upperBound, returnValue).

/**
 * This function checks to see whether or not a player's hand is a straight.
 * @param {int} index The starting index of a player's hand.
 * @param {int} upperBound The upper bound index of a player's hand. 
 * @returns Boolean true if the player's hand is a straight, otherwise boolean false if the player's hand is not a straight.
 */
function checkStraight(index, upperBound)
{
    // Declarations and initializations.

    let valueTracker = obtainCardValue(index); // Assign the value of the first card of the player's hand to a value tracker.

    // Execution.

    if (valueTracker == 14) // If the card at the first index is an ace...
    {
        let shouldReturn = true; // Establish a tracker for whether or not true should be returned after the loop.
        valueTracker = 1; // Set valueTracker to be one.

        for (let i = index + 4; i > index; i--) // Loop through the last four cards of the player's hand from right to left.
        {
            let cardValue = obtainCardValue(i);

            if (cardValue != (valueTracker + 1)) // If the card value at the current index is not what the next value should be...
            {
                shouldReturn = false; // Make it so that true won't be returned after the loop is broken out of.
                valueTracker = 14; // Set valueTracker to be 14.
                break; // Stop looping.
            } // End if (cardValue != (valueTracker + 1))

            valueTracker = cardValue; // Update valueTracker to be cardTracker's value.
        } // End for (let i = index + 4; i > index; i--).

        if (shouldReturn) // If true should be returned...
            return true; // Return true to indicate that the hand is a straight.
    } // End if (valueTracker == 14).

    for (let i = index + 1; i < upperBound; i++) // Loop through the last four cards of the player's hand.
    {
        let cardValue = obtainCardValue(i);

        if (cardValue != (valueTracker - 1)) // If the card value at the current index is not what the next value should be...
            return false; // Return false to indicate that the hand is not a straight.

        valueTracker = cardValue; // Update valueTracker to be cardTracker's value.
    } // End for (let i = index + 1; i < upperBound; i++).

    return true; // Return true to indicate that the hand is a straight.
} // End function checkStraight(index, upperBound).

/**
 * Returns the proper value of a straight.
 * @param {int} index The starting index of a player's hand.
 * @param {int} upperBound The upper bound index of a player's hand.
 * @param {int} valueToAdd The integer value to add to the return statement.
 * @returns The hand of the player represented numerically.
 */
function returnStraight(index, upperBound, valueToAdd)
{
    let highCardValue = obtainCardValue(index); // Obtain the first card's value and assign it to a temporary variable.

    if (highCardValue == 14 && cardArray[upperBound - 1].charAt(2) == '2') // If the first card in the player's hand is an ace and the last card is a two...
    {
        highCardValue = 5; // Set the high card value to five.

        for (let i = index; i < upperBound - 1; i++) // Loop through the first five cards of the player's hand.
            swap(i, i + 1); // Swap the card at the current index with the card at the next index.
    } // End if (highCardValue == 14 && cardArray[upperBound - 1].charAt(2) == '2').

    return valueToAdd + highCardValue; // Return XYY to indicate that the player has a straight (flush).
} // End function returnStraight(index, upperBound, valueToAdd).

/**
 * This function checks to see whether or not a player has obtained a two pair or a pair.
 * @param {int} index The starting index of a player's hand.
 * @param {int} upperBound The upper bound index of a player's hand.
 * @returns The hand of the player represented numerically.
 */
function pairCheck(index, upperBound)
{
    // Declarations and initializations.

    let valueTracker = cardArray[index].charAt(2); // Copy the value of the first card to a value tracking variable.
    let repeatCounter = 1; // Establish a tracker for the number of repeated cards.
    let secondaryRepeatCounter = 0; // Establish secondary repeat tracking variable for if it is required.

    // Execution.

    for (let i = index + 1; i < upperBound; i++) // Loop through the last four cards o f the player's hand.
    {
        // Handle repeatCounter.

        if (cardArray[i].charAt(2) == valueTracker) // If the value of the card at the current index is equal to that of valueTracker.
            repeatCounter++; // Increase the repeat counter.

        else // if the card wasn't repeated...
        {
            repeatCounter = 1; // Reset the repeat counter.
            valueTracker = cardArray[i].charAt(2); // Copy the value of the first card to the value tracking variable.
        } // End else.
    
        // Check for if the player has a two pair or a pair.

        if (repeatCounter == 2) // If a pair has been found...
            if (secondaryRepeatCounter != 0) // If a pair has already been found...
                return 200 + obtainCardValue(secondaryRepeatCounter); // Return 2YY to indicate that the player had a two pair.

            else // If a pair hasn't been found previously...
                secondaryRepeatCounter = i;

        if (i == upperBound - 1) // If it's the last card in the player's hand.
            if (secondaryRepeatCounter != 0) // If a pair ahs already been found...
                return 100 + obtainCardValue(secondaryRepeatCounter); // Return 1YY to indicate that the player had a pair.
    } // for (let i = index + 1; i < upperBound; i++).

    return 0; // Return zero to indicate that no pairs occurred.
} // End function pairCheck(index, upperBound).

/**
 * This function attempts to break ties between players relating to two pair hands.
 * @param {int} initialValue The intial two pair high card value as an integer.
 */
function twoPairTiebreak(initialValue)
{
    // Declarations and initializations.

    let playerOneScore = obtainCardValue(3); // Obtain the value of the fourth card in the array.
    let playerTwoScore = obtainCardValue(8); // Obtain the value of the ninth card in the array.
    initialValue %= 100; // Ensure that only the card value is the initial value.

    // Execution.

    if (playerOneScore > playerTwoScore) // If player one won...
        playerWins[0]++; // Increment the win counter for player one.

    else if (playerOneScore < playerTwoScore) // If player two won...
        playerWins[1]++; // Increment the win counter for player two.

    else // If the players are tied...
    {
        let secondaryValue = playerOneScore; // Store the secondary pair value.

        // Obtain high cards.

        // Player one.

        playerOneScore = obtainCardValue(0); // Obtain the value of player one's first card.

        if (playerOneScore == initialValue) // If the first card is the same as the initial tie value.
        {
            playerOneScore = obtainCardValue(2); // Obtain the value of player one's third card.
            if (playerOneScore == initialValue || playerOneScore == secondaryValue) // If the first index is the same as the initial tie value or the secondary tie value.
                playerOneScore = obtainCardValue(4); // Obtain the value of player one's last card.
        } // End if (playerOneScore == initialValue).

        // Player two.

        playerTwoScore = obtainCardValue(5); // Obtain the value of player two's first card.

        if (playerTwoScore == initialValue) // If the first card is the same as the initial tie value.
        {
            playerTwoScore = obtainCardValue(7); // Obtain the value of player two's third card.
            if (playerTwoScore == initialValue || playerTwoScore == secondaryValue) // If the first index is the same as the initial tie value or the secondary tie value.
                playerTwoScore = obtainCardValue(9); // Obtain the value of player two's last card.
        } // End if (playerOneScore == initialValue).

        // Compare the obtained high cards.

        if (playerOneScore > playerTwoScore) // If player one won...
            playerWins[0]++; // Increment the win counter for player one.

        else if (playerOneScore < playerTwoScore) // If player two won...
            playerWins[1]++; // Increment the win counter for player two.
    } // End else.
} // End function twoPairTiebreak()

/**
 * This function attempts to break ties between players relating to pair hands.
 * @param {int} initialValue The intial pair high card value as an integer.
 */
function pairTieBreak(initialValue)
{
    // Declarations and initializations.

    let highCardPosition = 0; // Establish a tracker for indexes in the to-be established arrays for player high cards.
    initialValue %= 100; // Ensure that only the card value is the initial value.

    // Obtain the high cards for player one.

    let playerOneIndividualCards = new Array(0, 0, 0); // Create an array to store the non-paired cards of player one.

    for (let i = 0; i < 5; i++) // Loop through player one's hand.
    {
        let cardValue = obtainCardValue(i); // Obtain the value of the card at the current index of the player's hand. 

        if (cardValue != initialValue) // If the obtained card value isn't the same as initialValue...
        {
            playerOneIndividualCards[highCardPosition] = cardValue; // Set the value in the playerOneIndividualCards array at the current highCardPosition index to be that of cardValue.
            highCardPosition++; // Increment highCardPosition.
        } // End if (cardValue != initialValue).
    } // End for (let i = 0; i < 5; i++).

    // Obtain the high cards for player two.

    let playerTwoIndividualCards = new Array(0, 0, 0); // Create an array to store the non-paired cards of player two.
    highCardPosition = 0; // Reset highCardPosition to zero.

    for (let i = 5; i < 10; i++) // Loop through player two's hand.
    {
        let cardValue = obtainCardValue(i); // Obtain the value of the card at the current index of the player's hand. 

        if (cardValue != initialValue) // If the obtained card value isn't the same as initialValue...
        {
            playerTwoIndividualCards[highCardPosition] = cardValue; // Set the value in the playerTwoIndividualCards array at the current highCardPosition index to be that of cardValue.
            highCardPosition++; // Increment highCardPosition.
        } // End if (cardValue != initialValue).
    } // End for (let i = 5; i < 10; i++).

    // Execution.

    for (let i = 0; i < 3; i++)
    {
        if (playerOneIndividualCards[i] > playerTwoIndividualCards[i]) // If player one won...
        {
            playerWins[0]++; // Increment the win counter for player one.
            return; // Stop execution of the function.
        } // End if (playerOneIndividualCards[i] > playerTwoIndividualCards[i]).

        else if (playerOneIndividualCards[i] < playerTwoIndividualCards[i]) // If player two won...
        {
            playerWins[1]++; // Increment the win counter for player two.
            return; // Stop execution of the function.
        } // End else if (playerOneIndividualCards[i] < playerTwoIndividualCards[i]).
    } // End for (let i = 0; i < 3; i++).
} // End function pairTieBreak(initialValue).

/**
 * This function attempts to determine the victor in the event of a high card tie between players.
 */
function highCardTieBreak()
{
    for (let i = 1; i < 5; i++) // Loop through the last four cards of both player's hands.
    {
        let playerOneScore = obtainCardValue(i); // Obtain the card value for player one at the current index.
        let playerTwoScore = obtainCardValue(i + 5); // Obtain the card value for player two at the current index.

        if (playerOneScore > playerTwoScore) // If player one won...
        {
            playerWins[0]++; // Increment the win counter for player one.
            return; // Stop execution of the function.
        } // End if (playerOneScore > playerTwoScore).

        else if (playerOneScore < playerTwoScore) // If player two won...
        {
            playerWins[1]++; // Increment the win counter for player two.
            return; // Stop execution of the function.
        } // End else if (playerOneScore < playerTwoScore).
    } // End for (let i = 1; i < 5; i++).
} // End function highCardTieBreak().

/**
 * This function converts hexidecimal values to decimal values.
 * @param {int} index The index of the card to be checked as an integer.
 * @returns The card's value in base 10 form.
 */
function obtainCardValue(index)
{
    if (cardArray[index].charCodeAt(2) > 57) // If the value is a letter.
        return 10 + cardArray[index].charCodeAt(2) - 65; // Retunn the number of the card in base 10.

    else // If the value is a number.
        return parseInt(cardArray[index].charAt(2)); // Return the number of the card in base 10.
} // End function obtainCardValue(index).
