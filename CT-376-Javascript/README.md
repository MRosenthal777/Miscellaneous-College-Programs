# CT-376: Javascript
The only program contained within this folder is my final project for this class. This program is a browser-based implementation of a game I made of the same name back in my high school game development class. Looking back on the program, there are two readily apparent things that come to mind which I could improve regarding performance:<br>
• Player hand rendering (it renders everything every time something changes instead of only what needs to be rendered)<br>
• The way in which the randomized music track is selected/loaded (string concatination can be used over if statements)

Other than that, improvements that could be made include mobile support, better support for screens of varying sizes, and better visual animations among other things.

I will likely modify this in the future though this initially commited version was the final state of the project which I submitted to the professor.
