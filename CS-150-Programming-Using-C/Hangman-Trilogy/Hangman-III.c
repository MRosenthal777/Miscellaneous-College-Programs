/*
  Student Name: Maxwell Rosenthal
  Course: CS-150 Programming using C
  Assignment: Hangman III
  Date: April 9th 2021
 */

// Inclusions.

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h> // Needed for time dependent RNG related elements.
#include <string.h>
#include <ctype.h> // Needed to remove the "implicit declaration of function 'toupper'" warning.

// Structure definitions.

typedef struct player // Player structure creation.
{
    char name[8]; //Stores a name up to 7 characters.
    short score;
} Player;

// Function headers.

Player game(char *word, Player player);
void displayStatistics(Player *leaderboard);
void acquireInput(char *enteredWord, bool *enteredLetters, short wordLength);
void addToLeaderboard(Player *leaderboard, Player playerToAdd);
short acquireWords(char *wordList[50]);
char * chooseWord(char *wordList[50], short wordCount, bool *generatedWords);
void populateLeaderboard(Player *leaderboard);
void updateStatistics(Player *leaderboard);
Player acquirePlayerInformation(Player *leaderboard);

/**
 * main() is essentially the core of this program. Not only does main() handle the menu, but main() stores the wordList which
 * comes from words.txt, the leaderboard from statistics.txt, as well as associated values and values needed to be passed
 * among multiple functions/through multiple iterations of a function.
 * 
 * @return int 
 */
int main()
{
    // Declarations and initializations.

    unsigned char decision;
    char *wordList[50]; // Size of array will be 50.
    short wordCount = acquireWords(wordList);
    bool generatedWords[50] = {false};
    Player leaderboard[50];
    populateLeaderboard(leaderboard);

    do
    {
        // Print the menu and collect user input.

        printf(" H  A  N  G  M  A  N\n=====================\n1. Play a Game\n2. Display Statistics\n3. Quit\n\nPlease enter an option.\n");

        while (scanf("%hhd", &decision) != 1) // Loops whenever scanf recieves input not equal to one character.
            printf("\nInvalid input, please enter a number between one and three.\n\n H  A  N  G  M  A  N\n=====================\n\
                1. Play a Game\n2. Display Statistics\n3. Quit\n\nPlease enter an option.\n"); // Prints menu again as to not break the flow of the program.

        // Menu functionality.

        switch (decision)
        {
            case 1: // Play a Game.
                getchar(); // Stops the newline character from the scanf() from interfering with the stream due to the fgets() function call in game().
                addToLeaderboard(leaderboard, game(chooseWord(wordList, wordCount, generatedWords), acquirePlayerInformation(leaderboard))); // Run the game() function and adds its returned player information to the leaderboard.
                decision = '\0'; // Null the stored decision value.
                break;

            case 2: // Display Statistics.
                displayStatistics(leaderboard);
                decision = '\0';
                break;

            case 3: // Quit.
                updateStatistics(leaderboard); // Update statistics.txt.
                for (short i = 0; i < wordCount; i++) // Free all elements in wordList.
                    free(wordList[i]);
                *wordList = NULL; // Null the wordList pointer.
                return(0);

            default: // Invalid input.
                printf("\nInvalid input, please enter a number between one and three.\n\n");
                decision = '\0';
                break;
        }
    } while (true); // Intentional infinite loop until 3 is entered thus ending the program.
}

/**
 * This function handles the game of hangman.
 * The program starts off by asking for the user's name.
 * From there, it will randomly select a word from a pre-defined list of 10 words stored in a 2D array of characters.
 * That word will be displayed as underscores and it is up to the player to play the game of hangman.
 * 
 * @param word A pointer to the word to be found in the iteration of the program (not particularly stored in main(), it just comes from chooseWord() and doesn't really get assigned to anything specific).
 * @param player The player structure to be appended to the leaderboard (if applicable) upon game completion.
 * @return Player
 */
Player game(char *word, Player player)
{
    // Generalized declarations and initializations.

    short wordLength = strlen(word);
    char wordTracker[wordLength + 1];
    short lettersRemaining = 0;

    // Populate the wordTracker and count up the letters needed to be found.

    for (short i = 0; i < wordLength; i++) // Populate the string with underscores among other characters.
        if (word[i] <= 90 && word[i] >= 65) // If a letter is present.
        {
            wordTracker[i] = '_';
            lettersRemaining++;
        }

        else // If a character like '?', ',', or ' ' is present.
            wordTracker[i] = word[i];

    wordTracker[wordLength] = '\0'; // Append the null terminator to the end of the string.

    // Let the games begin!

    {
        // Game related variable declarations and initializations.

        bool enteredLetters[26] = {false}, incorrectLetters[26] = {false};
        short attempts = 10;

        // Run the game.

        while (attempts > 0) // Run until the player runs out of attempts.
        {
            // Print the game information.

            printf("\nAttempts remaining: %d\
                \nScore: %d\
                \nAvailable Letters: ", 
                attempts, player.score);

            for (short i = 0; i < 26; i++) // Print the available letters with characters separated by spaces.
                if (!enteredLetters[i]) // If the index of the number corresponding to the entered letter is false.
                    printf("%c ", i + 65);

            printf("\nIncorrect Letters: ");

            for (short i = 0; i < 26; i++) // Print the incorrect letters with characters separated by spaces.
                if (incorrectLetters[i]) // If the index of the number corresponding to the incorrect letter is true.
                    printf("%c ", i + 65);

            printf("\n\n");

            for (short i = 0; i < wordLength; i++) // Print the dashed line wordTracker with characters separated by spaces.
                printf("%c ", wordTracker[i]);

            printf("\n\n");

            // Obtain a word guess or letter from the user and check for correctness.

            char enteredWord[32];

            acquireInput(enteredWord, enteredLetters, wordLength);

            if (strlen(enteredWord) == 1) // If letter.
            {
                // Mark letter as entered.

                enteredLetters[enteredWord[0] - 65] = true;

                // Check for the entered letter.

                bool letterFound = false;

                for (short i = 0; i < wordLength; i++) // Iterate through every character for correctness.
                    if (word[i] == enteredWord[0]) // If letter entered matches the letter at the current index.
                    {
                        player.score += 10; // Letter found, +10 points.
                        wordTracker[i] = enteredWord[0]; // Replace underscore at index i with the correct letter.
                        lettersRemaining--;
                        letterFound = true;
                    }

                if (!letterFound) // letterFound is false.
                {
                    player.score -= 2; // Letter not found, -2 points.
                    attempts--;
                    incorrectLetters[enteredWord[0] - 65] = true;
                }

                if (lettersRemaining == 0) // If the word is completed.
                {
                    player.score += 100; // Word found, +100 points.

                    printf("\n%s\n\nCongratulations, %s! You solved it! Your score is %d points!\n\n", word, player.name, player.score);

                    return player;
                }
            }

            else // If word.
            {
                if (!strcmp(enteredWord, word)) // If the word guess is correct.
                {
                    player.score += 200; // Word entered as guess and found, +200 points.
                    player.score += 100; // Word found, +100 points.

                    printf("\n%s\n\nCongratulations, %s! Your word guess was correct! Your score is %d points!\n\n", word, player.name, player.score);

                    return player;
                }

                else // If the word guess is incorrect.
                {
                    player.score -= 80; // Word entered as guess but not found, -80 points.
                    attempts--;

                    printf("\nSorry %s, your word guess was incorrect.\n", player.name);
                }
            }
        }
    }

    // Bad end, the player did not find the word.

    printf("\n%s\n\n", word);

    player.score -= 40; // Word not found, -40 points.

    printf("Game over! I'm sorry, %s, your final score is %d points.\n\n", player.name, player.score);

    return player;
}

/**
 * This function asks a user to enter a letter or a word.
 * Entered input is made uppercase and validated before being sent back to game().
 * If invalid, the function will be recursively called until correct input is entered.
 * Invalid input is anything that's not a letter or the same size as the word to guess.
 * 
 * @param enteredWord a pointer to the enteredWord array stored in game() that stores the entered word.
 * @param enteredLetters a pointer to the enteredLetters array stored in game() that logs the entered characters.
 * @param wordLength contains the length of the word to be guessed.
 */
void acquireInput(char *enteredWord, bool *enteredLetters, short wordLength)
{
    // Acquire input and record the entered string length.

    printf("Enter a letter to search for or guess the current word.\n");

    while (fgets(enteredWord, 32, stdin) == NULL) // Loops whenever fgets recieves an invalid input.
        printf("\nInvalid input. Enter a letter to search for or guess the current word.\n");

    if (enteredWord[(int) strlen(enteredWord) - 1] == '\n') // If the maximum string length wasn't entered this will run as '\n' will be appended to the end.
        enteredWord[(int) strlen(enteredWord) - 1] =  '\0';

    else // If the maximum string length is entered, this will run to clear the stream of the remaining '\n' as to not interfere with future fgets() calls.
        getchar();

    short enteredLen = (short) strlen(enteredWord);

    // Check for output type and execute.

    if (enteredLen == 1) // If character.
    {
        // Convert to upper case.

        enteredWord[0] = toupper(enteredWord[0]);

        // Ensure the character is an unused capital letter.

        if (enteredLetters[enteredWord[0] - 65] || !isalpha(enteredWord[0])) // If an already entered letter or non-letter character. 
        {
            printf("\nThe character you entered must be a letter and that letter cannot have been previously entered.\n\n");
            acquireInput(enteredWord, enteredLetters, wordLength);
        }
    }

    else if (enteredLen != wordLength) // If the entered word is not the same length as the word to guess.
    {
        printf("\nYour guess isn't the same length as the word. Please enter a letter or a valid guess.\n\n");
        acquireInput(enteredWord, enteredLetters, wordLength);
    }

    else // If valid word.
        for (short i = 0; i <= enteredLen; i++) // Ensure every entered letter is uppercase.
            enteredWord[i] = toupper(enteredWord[i]);
}

/**
 * This function displays a 50 person leaderboard for the game... Enough said.
 * 
 * @param leaderboard a pointer to the leaderboard array stored in main().
 */
void displayStatistics(Player *leaderboard)
{
    if (leaderboard[0].score == -1) // If the leaderboard has no entries.
        printf("\nThe leaderboard is currently empty.\n");

    else // If the leaderboard has at least one entry.
    {
        // Print the leaderboard column labels and separator.

        printf("\nRank\t Name\t   Score\n========================\n");

        // Print the leaderboard values.

        for (short i = 0; i < 50; i++) // Iterate through the leaderboard values.
            if (leaderboard[i].score != -1) // If the score of the current index isn't -1.
                printf("%d\t%s\t%8d\n", i + 1, leaderboard[i].name, leaderboard[i].score);

            else // The current index's score is -1.
                break;
    }

    printf("\n");
}

/**
 * This function adds a player to the leaderboard.
 * 
 * @param leaderboard a pointer to the leaderboard array stored in main().
 * @param playerToAdd the scructure that handles the player to add to the leaderboard.
 */
void addToLeaderboard(Player *leaderboard, Player playerToAdd)
{
    for(short i = 0; i < 50; i++) // Add playerToAdd to the leaderboard. If 50 slots are exceeded, the loop stops thus deleting whatever is in 51st place on the leaderboard.
    {
        if (leaderboard[i].score == -1) // If the current record slot is empty, add the player to it and break.
        {
            leaderboard[i] = playerToAdd;
            break;
        }

        else if (playerToAdd.score > leaderboard[i].score) // If the player to add to the leaderboard's score is higher than the score at the current index, swap the index's value with playerToAdd.
        {
            Player tempPlayer = leaderboard[i];
            leaderboard[i] = playerToAdd;
            playerToAdd = tempPlayer;
        }
    }
}

/**
 * This function acquires words from words.txt and stores them in the dynamic array wordList stored in main().
 * Various parts of this code are based on the code of the Week 14 lessons.
 * 
 * @param wordlist a pointer to the wordList array stored in main() that dynamically stores the words in words.txt.
 * @return short
 */
short acquireWords(char *wordList[50])
{
    // Declarations and initializations.

    FILE *wordListFilePointer = fopen("words.txt", "r");

    if (!wordListFilePointer) // If wordListFilePointer is NULL.
    {
        printf("\nwords.txt could not be opened for reading.");
        exit(EXIT_FAILURE);
    }

    short wordCount = 0;
    char tempWord[31]; // Stores a 30 character maximum word.

    // Read from the file line by line.

    for (; wordCount < 50; wordCount++) // Reads in 50 lines.
        if (fgets(tempWord, sizeof(tempWord), wordListFilePointer) != NULL) // If something is read.
        {
            if (tempWord[(int) strlen(tempWord) - 1] == '\n') // If the maximum string length wasn't entered this will run as '\n' will be appended to the end.
                tempWord[(int) strlen(tempWord) - 1] =  '\0';

            else // If the maximum string length is entered, this will run to clear the stream of the remaining '\n' as to not interfere with future fgets() calls.
                fgetc(wordListFilePointer);

            wordList[wordCount] = (char *) malloc(strlen(tempWord) + 1);

            strcpy(wordList[wordCount], tempWord);

            if (!wordList[wordCount]) // If the copied word wasn't copied.
            {
                printf("\nNo more memory available!");
                exit(EXIT_FAILURE);
            }
        }

        else // If nothing is read.
            break;

    // Close the file and null the pointer.

    fclose(wordListFilePointer);
    wordListFilePointer = NULL;

    return wordCount;
}

/**
 * Randomly generate a number that will correspond to a word from a list of words titled wordList.
 * Random number generation code was provided in Hangman-I.
 * 
 * @param wordList a pointer to the wordList array stored in main().
 * @param wordCount the amount of words stored in wordList.
 * @param generatedWords a pointer to the generatedWords array stored in main() that records words already generated. If it becomes entirely true, it will be reset to false.
 * @return char* 
 */
char * chooseWord(char *wordList[50], short wordCount, bool *generatedWords)
{
    // Check if all the values have been used before.

    {
        short trueCount = 0;

        for (short i = 0; i < wordCount; i++) // Determine if everything is true or there are falses.
        {
            if (generatedWords[i]) // If true.
                trueCount++;

            else // If false.
                break;
        }

        if (trueCount == wordCount) // If everything is true.
            for (short i = 0; i < wordCount; i++) // Reset the array to be entirely false.
                generatedWords[i] = false;
    }

    // Generate a random number.

    time_t t; // Time related value to be passed.

    srand((unsigned) time(&t)); // Randomize the seed based on time.

    short generatedNumber = rand() % wordCount; // generates a number corresponding to a random index from 0 to wordCount - 1.

    {
        short i = 0;

        while (generatedWords[generatedNumber] && i < 5) // If the generated number corresponds to a true value and the loop hasn't been run more than five times.
        {
            generatedNumber = rand() % wordCount;
            i++;
        }
    }

    if (generatedWords[generatedNumber]) // If still a number that's been encountered before.
        for (short i = 0; i < wordCount; i++) // Iterate through values until the first false instance is found.
            if (!generatedWords[i]) // If a false instance is found.
            {
                generatedNumber = i; // Assign the index of the first false index to the generatedNumber.
                break;
            }

    generatedWords[generatedNumber] = true;

    return wordList[generatedNumber]; // Returns the word at the index of the generated number.
}

/**
 * This function populates the leaderboard array stored in main() with the data stored in statistics.txt.
 * Various parts of this code are based on the code of the Week 14 lessons.
 * 
 * @param leaderboard a pointer to the leaderboard array stored in main().
 */
void populateLeaderboard(Player *leaderboard)
{
    // Declarations and initializations.

    FILE *statisticsFilePointer = fopen("statistics.txt", "r");

    if (!statisticsFilePointer) // If statisticsFilePointer is NULL.
    {
        printf("\nstatistics.txt could not be opened for reading.");
        exit(EXIT_FAILURE);
    }

    {
        char tempRecord[15]; // Stores a 14 character maximum entry.

        short i = 0;

        // Populate the leaderboard.

        for (; i < 50; i++) // Add entries from statistics.txt to leaderboard.
        {
            if (fgets(tempRecord, sizeof(tempRecord), statisticsFilePointer) != NULL) // If something is read.
            {
                if (tempRecord[(int) strlen(tempRecord) - 1] == '\n') // If the maximum string length wasn't entered this will run as '\n' will be appended to the end.
                    tempRecord[(int) strlen(tempRecord) - 1] =  '\0';

                else // If the maximum string length is entered, this will run to clear the stream of the remaining '\n' as to not interfere with future fgets() calls.
                    fgetc(statisticsFilePointer);

                sscanf(tempRecord, "%[^|]|%hd", leaderboard[i].name, &leaderboard[i].score);

                if (!strcmp(leaderboard[i].name, "")) // If the copied wasn't populated.
                {
                    printf("\nNo more memory available!");
                    exit(EXIT_FAILURE);
                }
            }

            else // If something is not read.
                break;
        }

        for (; i < 50; i++) // Assign any remaining values to have -1 as their score.
            leaderboard[i].score = -1;
    }

    // Close the file and null the pointer.

    fclose(statisticsFilePointer);
    statisticsFilePointer = NULL;
}

/**
 * This function updates statistics.txt upon program exit with the contents of the leaderboard array.
 * Various parts of this code are based on the code of the Week 14 lessons.
 * 
 * @param leaderboard a pointer to the leaderboard array stored in main().
 */
void updateStatistics(Player *leaderboard)
{
    // Declarations and initializations.

    FILE *statisticsFilePointer = fopen("statistics.txt", "w");

    if (!statisticsFilePointer) // If statisticsFilePointer is NULL.
    {
        printf("\nstatistics.txt could not be opened for writing.");
        exit(EXIT_FAILURE);
    }

    {
        short i = 0;

        // Update statistics.txt.

        for (; i < 50; i++) // Add entries from statistics.txt to leaderboard.
            if (leaderboard[i].score != -1) // If the current index's score isn't -1.
                fprintf(statisticsFilePointer, "%s|%hd\n", leaderboard[i].name, leaderboard[i].score);

            else // If the current index's score is -1.
                break;
    }

    // Close the file and null the pointer.

    fclose(statisticsFilePointer);
    statisticsFilePointer = NULL;
}

/**
 * This function asks the user to enter their name. If their name is present in the leaderboard, their statistics will be returned
 * and everything after their entry will be shifted up until the end of the array or until a null value is encountered.
 * If their name is not in the leaderboard, a Player structure with their entered name and a score of zero will be returned.
 * 
 * @param leaderboard a pointer to the leaderboard array stored in main().
 * @return Player 
 */
Player acquirePlayerInformation(Player *leaderboard)
{
    // Player creation and name acquisition.

    Player player = {"", 0};

    printf("\nPlease enter your name.\n");

    while (fgets(player.name, 8, stdin) == NULL) // Loops whenever fgets recieves an invalid input.
        printf("\nInvalid input. Please enter your name.\n");

    if (player.name[(int) strlen(player.name) - 1] == '\n') // If the maximum string length wasn't entered this will run as '\n' will be appended to the end.
        player.name[(int) strlen(player.name) - 1] =  '\0';

    else // If the maximum string length is entered, this will run to clear the stream of the remaining '\n' as to not interfere with future fgets() calls.
        getchar();

    // Determine if the player is in the leaderboard. If so, adopt their score and remove their index from the leaderboard, shifting anything after them up one place.

    for (short i = 0; i < 50; i++) // Find if the player entered is on the leaderboard.
    {
        if (leaderboard[i].score != -1) // If the current index is not null.
        {
            if (!strcmp(leaderboard[i].name, player.name)) // If the player is on the leaderboard.
            {
                // Copy the score at the current index to the Player player.

                player.score = leaderboard[i].score;

                for (; i < 49; i++) // Move all indexes up by one as to replace the index just removed.
                    if (leaderboard[i].score != -1) // If the current index is not a null index.
                        leaderboard[i] = leaderboard[i + 1];

                    else // If the current index is a null index;
                        return player;

                // Null index 49.

                strcpy(leaderboard[49].name, "");
                leaderboard[49].score = -1;

                return player;
            }
        }

        else // If the current index is null.
            return player;
    }

    return player;
}
