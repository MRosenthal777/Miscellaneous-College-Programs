/*
  Student Name: Maxwell Rosenthal
  Course: CS-150 Programming using C
  Assignment: Hangman I
  Date: February 19th 2021
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h> // Needed for time dependent RNG related elements.

// Function definitions.
void game();
char acquireInput(bool enteredCharacters[26]);

/**
 * In this program, main() is essentially the menu of the program.
 * In this menu there are three options:
 * 1. Play a Game - This runs the game() function which is the actual game itself.
 * 2. Display Statistics - This displays statistics. It is currently not implemented in this iteration of the program.
 * 3. Quit - This ends the program.
 * 
 * @return int 
 */
int main()
{
    // Declare the loop controlling value.
    unsigned char decision = 0;

    do // do while loop because I never use them.
    {
        // Print menu.
        printf(" H  A  N  G  M  A  N\n=====================\n1. Play a Game\n2. Display Statistics\n3. Quit\n\nPlease enter an option.\n");

        // Collect user input.
        scanf(" %hhd", &decision);

        // Menu switch statement (Because the instructions asked for one. I initially wanted to use an if else if wall.).
        switch (decision)
        {
            case 1:
                game();
                decision = 0; // I am resetting the decisions after they are made because I ran into some issues in testing if I didn't.
                break;
            case 2:
                printf("\nCurrently not implemented at this time. Please choose another option.\n\n");
                decision = 0;
                break;
            case 3:
                return(0); // Technically doing things this way means the loop will never end... before taking off points, keep in mind that it's more efficent this way.
            default:
                printf("\nInvalid option, please enter a number between one and three.\n\n");
                break;
        }
    } while (decision != 3);
}

/**
 * This function handles the game of hangman.
 * The program starts off by asking for the user's name.
 * From there, it will randomly select a word from a pre-defined list of 10 words stored in a 2D array of characters.
 * That word will be displayed as underscores and it is up to the player to play the game of hangman. 
 */
void game()
{
    // Player name acquisition.

    // Initial code provided by instructor, modified by me to be how I would do it.

    char username[11]; //Stores a name up to 10 characters.
    //The extra element is for the '\0' (string terminator).

    printf("\nWhat is your name?\n");
    scanf(" %s", username);

    printf("\nYour name is %s.\n", username);

    // Randomly choose a word form a 2D array of characters.

    // Code uses modified lines from provided source.

    time_t t; // Time related value to be passed.
    srand((unsigned) time(&t)); // Randomize seed based on time.
    const unsigned char row = rand() % 10; // Store a number from 0 to 9 as an unsigned char.

    // Declare and initialize the wordList.

    // I probably could define this as WORD_LIST though that caused issues when I tried it and it may count as a global variable.
    const char wordList[10][30] =
    {
        {'W', 'O', 'R', 'D', '\0', 'Z', 'E', 'R', 'O'}, // Word zero.
        {'W', 'O', 'R', 'D', '\0', 'O', 'N', 'E'}, // Word one.
        {'W', 'O', 'R', 'D', '\0', 'T', 'W', 'O'}, // Word two.
        {'W', 'O', 'R', 'D', '\0', 'T', 'H', 'R', 'E', 'E'}, // Word three.
        {'W', 'O', 'R', 'D', '\0', 'F', 'O', 'U', 'R'}, // Word four.
        {'W', 'O', 'R', 'D', '\0', 'F', 'I', 'V', 'E'}, // Word five.
        {'W', 'O', 'R', 'D', '\0', 'S', 'I', 'X'}, // Word six.
        {'W', 'O', 'R', 'D', '\0', 'S', 'E', 'V', 'E', 'N'}, // Word seven.
        {'W', 'O', 'R', 'D', '\0', 'E', 'I', 'G', 'H', 'T'}, // Word eight.
        {'W', 'O', 'R', 'D', '\0', 'N', 'I', 'N', 'E'} // Word nine.
    };

    // Dashed line word tracker creation.

    char wordTracker[30] = {'\0'};
    short lettersRemaining = 0;

    for (int i = 0; i < 30; i++)
        if (wordList[row][i] <= 90 && wordList[row][i] >= 65)
        {
            wordTracker[i] = '_';
            lettersRemaining++;
        }
        else if (wordList[row][i] != 0)
            wordTracker[i] = wordList[row][i];

    // Let the games begin!

    bool letterFound, enteredCharacters[26] = {false}; // For whatever reason if I did not initialize enteredCharacters as false some things would have numerical values making some things true by default.
    char character;
    short score = 0, attempts = 10, maxScore = lettersRemaining * 10 + 100;

    while (attempts > 0)
    {
        // Print dashed line and general information.
        printf("\nAttempts remaining: %d\nScore: %d\n", attempts, score);

        for (int i = 0; i < 30; i++)
            printf("%c ", wordTracker[i]);
        printf("\n\n");

        // Setup check.
        character = acquireInput(enteredCharacters);

        // Mark the character as entered.
        // This could be done inside the function though recursion would make the line run more times than necessary in some cases.
        enteredCharacters[character - 'A'] = true;
        letterFound = false;

        // Check letter.
        for (int i = 0; i < 30; i++)
            if (wordList[row][i] == character)
            {
                score += 10; // Letter found, +10 points.
                wordTracker[i] = character;
                lettersRemaining--;
                letterFound = true;
            }

        // Letter not found.
        if (!letterFound)
        {
            score -= 2; // Letter not found, -2 points.
            attempts--;
        }

        // Run if word completed.
        if (lettersRemaining == 0)
        {
            // Print the completed word.
            printf("\n");
            for (int i = 0; i < 30; i++)
                printf("%c ", wordList[row][i]);
            printf("\n\n");

            // Congratulate the user and display their final score (current score + 100 points).
            printf("Congratulations, %s! You solved it! You earned a score of %d out of %d points!\n\n", username, score + 100, maxScore);

            return;
        }
    }

    // Bad end, player didn't find the word.

    // Print the completed word.
    printf("\n");
    for (int i = 0; i < 30; i++)
        printf("%c ", wordList[row][i]);
    printf("\n\n");

    // Tell the user game over and display their final score (current score - 40 points).
    printf("Game over! I'm sorry, %s, your final score is %d out of %d points.\n\n", username, score - 40, maxScore);
}

/**
 * This function asks a user to enter a letter.
 * Once a character is entered, if that character is above the ASCII range for capital letters it will have 32 subtracted from it.
 * By doing this, any inputted lowercase characters should be converted to uppercase.
 * Before returning the character though, a recursive check will be preformed in order to ensure that a letter was entered.
 * This is done by a simple if statement that checks whether or not a character is in fact in the end uppercase letter ASCII range.
 * 
 * @return char
 */
char acquireInput(bool enteredCharacters[26])
{
    char character;

    // Acquire input.
    printf("Enter a letter to search for.\n");
    scanf(" %c", &character);

    // Convert to upper case if above uppercase ASCII range.
    if (character > 'Z')
        character -= 32;

    // Ensure the character is an unused capital letter (haha, recursion go brrrrr).
    if (character > 'Z' || character < 'A' || enteredCharacters[character - 'A'])
    {
        printf("\nYou must enter a letter and that letter cannot have been previously entered.\n\n");
        character = acquireInput(enteredCharacters);
    }

    return character;
}
