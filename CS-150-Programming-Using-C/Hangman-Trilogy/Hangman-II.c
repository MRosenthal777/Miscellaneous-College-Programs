/*
  Student Name: Maxwell Rosenthal
  Course: CS-150 Programming using C
  Assignment: Hangman II
  Date: April 2nd 2021
 */

// Inclusions.

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h> // Needed for time dependent RNG related elements.
#include <string.h>
#include <ctype.h> // Needed to remove the "implicit declaration of function 'toupper'" warning.

// Structure definitions.

typedef struct player // Player structure creation.
{
    char name[8]; //Stores a name up to 7 characters.
    short score;
} Player;

// Function definitions.

Player game();
void displayStatistics(Player *leaderboard);
void acquireInput(char *enteredWord, bool *enteredLetters, int wordLength);
void addToLeaderboard(Player *leaderboard, Player playerToAdd);

/**
 * In this program, main() is essentially the menu of the program and handles the storage of the leaderboard.
 * In this menu there are three options:
 * 1. Play a Game - This runs the game() function which is the actual game itself and once completed, updates the leaderboard based on what game() returns.
 * 2. Display Statistics - This displays the leaderboard statistics. Entries not present are indicated with -1 as their score as -1 is not a possible value to attain in-game.
 * 3. Quit - This ends the program.
 * 
 * @return int 
 */
int main()
{
    // Declarations and initializations.

    unsigned char decision;
    Player leaderboard[50];

    for (int i = 0; i < 50; i++) // To my knowledge this is the cleanest way to assign default score values other than 0 to all the entries in leaderboard.
        leaderboard[i].score = -1;

    do
    {
        // Print the menu and collect user input.

        printf(" H  A  N  G  M  A  N\n=====================\n1. Play a Game\n2. Display Statistics\n3. Quit\n\nPlease enter an option.\n");

        while (scanf("%hhd", &decision) != 1) // Loops whenever scanf recieves input not equal to one character.
            printf("\nInvalid input, please enter a number between one and three.\n\n H  A  N  G  M  A  N\n=====================\n\
                1. Play a Game\n2. Display Statistics\n3. Quit\n\nPlease enter an option.\n"); // Prints menu again as to not break the flow of the program.

        // Menu functionality.

        switch (decision)
        {
            case 1: // Play a Game.
                getchar(); // Stops the newline character from the scanf() from interfering with the stream due to the fgets() function call in game().
                addToLeaderboard(leaderboard, game()); // Runs the game() function and adds its returned player information to the leaderboard.
                decision = '\0'; // Null the stored decision value.
                break;

            case 2: // Display Statistics.
                displayStatistics(leaderboard);
                decision = '\0';
                break;

            case 3: // Quit.
                return(0);

            default: // Invalid input.
                printf("\nInvalid input, please enter a number between one and three.\n\n");
                decision = '\0';
                break;
        }
    } while (true); // Intentional infinite loop until 3 is entered thus ending the program.
}

/**
 * This function handles the game of hangman.
 * The program starts off by asking for the user's name.
 * From there, it will randomly select a word from a pre-defined list of 10 words stored in a 2D array of characters.
 * That word will be displayed as underscores and it is up to the player to play the game of hangman.
 * 
 * @return Player
 */
Player game()
{
    // Player creation and name acquisition.

    Player player = {"", 0};

    printf("\nPlease enter your name.\n");

    while (fgets(player.name, 8, stdin) == NULL) // Loops whenever fgets recieves an invalid input.
        printf("\nInvalid input. Please enter your name.\n");

    if (player.name[(int) strlen(player.name) - 1] == '\n') // If the maximum string length wasn't entered this will run as '\n' will be appended to the end.
        player.name[(int) strlen(player.name) - 1] =  '\0';

    else // If the maximum string length is entered, this will run to clear the stream of the remaining '\n' as to not interfere with future fgets() calls.
        getchar();

    // Randomly generate a number that will correspond to a word from a list of words titled wordList.

    time_t t; // Time related value to be passed.
    srand((unsigned) time(&t)); // Randomize seed based on time.
    const unsigned char wordIndex = rand() % 10; // Store a number from 0 to 9 as an unsigned char.

    // Declare and initialize the wordList and store the length of the randomly seleted word as wordLength.

    const char *wordList[] =
    {
        "WORD ZERO", // Word zero.
        "WORD ONE", // Word one.
        "WORD TWO", // Word two.
        "WORD THREE", // Word three.
        "WORD FOUR", // Word four.
        "WORD FIVE", // Word five.
        "WORD SIX", // Word six.
        "WORD SEVEN", // Word seven.
        "WORD EIGHT", // Word eight.
        "WORD NINE", // Word nine.
    };

    short wordLength = strlen(wordList[wordIndex]);

    // Declare the dashed line wordTracker char array and the lettersRemaining short with initialization applied to the ladder.

    char wordTracker[wordLength + 1];
    short lettersRemaining = 0;

    // Populate the wordTracker and count up the letters needed to be found.

    for (int i = 0; i < wordLength; i++) // Populate the string with underscores among other characters.
        if (wordList[wordIndex][i] <= 90 && wordList[wordIndex][i] >= 65) // If a letter is present.
        {
            wordTracker[i] = '_';
            lettersRemaining++;
        }

        else // If a character like '?', ',', or ' ' is present.
            wordTracker[i] = wordList[wordIndex][i];

    wordTracker[wordLength] = '\0'; // Append the null terminator to the end of the string.

    // Let the games begin!

    {
        // Declare and initialize recurring game related variables.

        bool enteredLetters[26] = {false}, incorrectLetters[26] = {false};
        short attempts = 10;

        // Run the game.

        while (attempts > 0)
        {
            // Print the game information.

            printf("\nAttempts remaining: %d\
                \nScore: %d\
                \nAvailable Letters: ", 
                attempts, player.score);

            for (int i = 0; i < 26; i++) // Prints available letters with characters separated by spaces.
                if (!enteredLetters[i])
                    printf("%c ", i + 65);

            printf("\nIncorrect Letters: ");

            for (int i = 0; i < 26; i++) // Prints incorrect letters with characters separated by spaces.
                if (incorrectLetters[i])
                    printf("%c ", i + 65);

            printf("\n\n");

            for (int i = 0; i < wordLength; i++) // Prints the dashed line wordTracker with characters separated by spaces.
                printf("%c ", wordTracker[i]);

            printf("\n\n");

            // Obtain a word guess or letter from the user and check for correctness.

            char enteredWord[32];

            acquireInput(enteredWord, enteredLetters, wordLength);

            if (strlen(enteredWord) == 1) // If letter.
            {
                // Mark letter as entered.

                enteredLetters[enteredWord[0] - 65] = true;

                // Check for the entered letter.

                bool letterFound = false;

                for (int i = 0; i < wordLength; i++) // Iterate through every character for correctness.
                    if (wordList[wordIndex][i] == enteredWord[0]) // If letter entered matches the letter at the current index.
                    {
                        player.score += 10; // Letter found, +10 points.
                        wordTracker[i] = enteredWord[0]; // Replace underscore at index i with the correct letter.
                        lettersRemaining--;
                        letterFound = true;
                    }

                if (!letterFound) // if letterFound is false.
                {
                    player.score -= 2; // Letter not found, -2 points.
                    attempts--;
                    incorrectLetters[enteredWord[0] - 65] = true;
                }

                if (lettersRemaining == 0) // Word is completed.
                {
                    player.score += 100; // Word found, +100 points.

                    printf("\n%s\n\nCongratulations, %s! You solved it! You earned %d points!\n\n", wordList[wordIndex], player.name, player.score);

                    return player;
                }
            }

            else // If word.
            {
                if (!strcmp(enteredWord, wordList[wordIndex])) // If word guess is correct.
                {
                    player.score += 200; // Word entered as guess and found, +200 points.
                    player.score += 100; // Word found, +100 points.

                    printf("\n%s\n\nCongratulations, %s! Your word guess was correct! You earned %d points!\n\n", wordList[wordIndex], player.name, player.score);

                    return player;        
                }

                else // If word guess is incorrect.
                {
                    player.score -= 80; // Word entered as guess but not found, -80 points.
                    attempts--;

                    printf("\nSorry %s, your word guess was incorrect.\n", player.name);
                }
            }
        }
    }

    // Bad end, player did not find the word.

    printf("\n%s\n\n", wordList[wordIndex]);

    player.score -= 40; // Word not found, -40 points.

    printf("Game over! I'm sorry, %s, your final score is %d points.\n\n", player.name, player.score);

    return player;
}

/**
 * This function asks a user to enter a letter or a word.
 * Entered input is made uppercase and validated before being sent back to game().
 * If invalid, the function will be recursively called until correct input is entered.
 * Invalid input is anything that's not a letter or the same size as the word to guess.
 * 
 * @param enteredWord a pointer to the enteredWord array stored in game() that stores the entered word.
 * @param enteredLetters a pointer to the enteredLetters array stored in game() that logs the entered characters.
 * @param wordLength contains the length of the word to be guessed.
 */
void acquireInput(char *enteredWord, bool *enteredLetters, int wordLength)
{
    // Acquire input and record the entered string length.

    printf("Enter a letter to search for or guess the current word.\n");

    while (fgets(enteredWord, 32, stdin) == NULL) // Loops whenever fgets recieves an invalid input.
        printf("\nInvalid input. Enter a letter to search for or guess the current word.\n");

    if (enteredWord[(int) strlen(enteredWord) - 1] == '\n') // If the maximum string length wasn't entered this will run as '\n' will be appended to the end.
        enteredWord[(int) strlen(enteredWord) - 1] =  '\0';

    else // If the maximum string length is entered, this will run to clear the stream of the remaining '\n' as to not interfere with future fgets() calls.
        getchar();

    short enteredLen = (short) strlen(enteredWord);

    // Check for output type and execute.

    if (enteredLen == 1) // If character.
    {
        // Convert to upper case.

        enteredWord[0] = toupper(enteredWord[0]);

        // Ensure the character is an unused capital letter.

        if (enteredLetters[enteredWord[0] - 65] || !isalpha(enteredWord[0])) // If an already entered letter or non-letter character. 
        {
            printf("\nThe character you entered must be a letter and that letter cannot have been previously entered.\n\n");
            acquireInput(enteredWord, enteredLetters, wordLength);
        }
    }

    else if (enteredLen != wordLength) // If the entered word is not the same length as the word to guess.
    {
        printf("\nYour guess isn't the same length as the word. Please enter a letter or a valid guess.\n\n");
        acquireInput(enteredWord, enteredLetters, wordLength);
    }

    else // If valid word.
        for (int i = 0; i <= enteredLen; i++) // Ensure every entered letter is uppercase.
            enteredWord[i] = toupper(enteredWord[i]);
}

/**
 * This function displays a 50 person leaderboard for the game... Enough said.
 */
void displayStatistics(Player *leaderboard)
{
    if (leaderboard[0].score == -1) // If the leaderboard has no entries.
        printf("\nThe leaderboard is currently empty.\n");

    else // If the leaderboard has at least one entry.
    {
        // Print the leaderboard column labels and separator.

        printf("\nRank\tName\tScore\n=====================\n");

        // Print the leaderboard values.

        for (int i = 0; i < 50; i++) // Iterate through the leaderboard values.
            if (leaderboard[i].score != -1) // If the score of the current index isn't -1.
                printf("%d\t%s\t%5d\n", i + 1, leaderboard[i].name, leaderboard[i].score);

            else // The current index's score is -1.
                break;
    }

    printf("\n");
}

/**
 * This function adds a player to the leaderboard.
 * 
 * @param leaderboard a pointer to the leaderboard array stored in main() that stores people's names and scores in order of score.
 * @param playerToAdd the scructure that handles the player to add to the leaderboard.
 */
void addToLeaderboard(Player *leaderboard, Player playerToAdd)
{
    for(int i = 0; i < 50; i++) // Add playerToAdd to the leaderboard. If 50 slots are exceeded, the loop stops thus deleting whatever is in 51st place on the leaderboard.
    {
        if (leaderboard[i].score == -1) // If the current record slot is empty, add the player to it and break.
        {
            leaderboard[i] = playerToAdd;
            break;
        }

        else if (playerToAdd.score > leaderboard[i].score) // If the player to add to the leaderboard's score is higher than the score at the current index, swap the index's value with playerToAdd.
        {
            Player tempPlayer = leaderboard[i];
            leaderboard[i] = playerToAdd;
            playerToAdd = tempPlayer;
        }
    }
}
